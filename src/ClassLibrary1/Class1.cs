﻿using System;

namespace ClassLibrary1
{
    public class Class1 : IClass
    {
        public bool ReturnArgument(bool argument)
        {
            return argument;
        }

        public bool ReturnFalse()
        {
            return false;
        }

        public bool ReturnTrue()
        {
            return true;
        }
    }

    public interface IClass
    {
        bool ReturnTrue();

        bool ReturnFalse();

        bool ReturnArgument(bool argument);
    }
}
