﻿using ClassLibrary1;
using System;

namespace HelloWorld_ContinouesIntegration
{
    class Program
    {
        static void Main(string[] args)
        {
            IClass class1 = new Class1();

            Console.WriteLine(class1.ReturnFalse());
            Console.WriteLine(class1.ReturnTrue());
            Console.WriteLine(class1.ReturnArgument(false));
            Console.WriteLine(class1.ReturnArgument(true));

            Console.ReadKey();
        }
    }
}
