using ClassLibrary1;
using NUnit.Framework;

namespace NUnitTestProject1
{
    public class Tests
    {
        [Test]
        public void ReturnTrue()
        {
            IClass class1 = new Class1();

            var result = class1.ReturnTrue();

            Assert.IsTrue(result);
        }

        [Test]
        public void ReturnFalse()
        {
            IClass class1 = new Class1();

            var result = class1.ReturnFalse();

            Assert.IsFalse(result);
        }

        [Test]
        public void ReturnArgumentTrue()
        {
            IClass class1 = new Class1();

            var result = class1.ReturnArgument(true);

            Assert.IsTrue(result);
        }

        [Test]
        public void ReturnArgumentFalse()
        {
            IClass class1 = new Class1();

            var result = class1.ReturnArgument(false);

            Assert.IsFalse(result);
        }
    }
}